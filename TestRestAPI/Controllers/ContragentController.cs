﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TestRestAPI.Helpers.DaData;
using TestRestAPI.Helpers.LiteDb;
using TestRestAPI.Models;

namespace TestRestAPI.Controllers
{
	/// <summary>
	/// Contragent Controller.
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	public class ContragentController : ControllerBase
	{
		private readonly ILiteDbContragentService _contragentDbService;
		private readonly IDaDataApiContragentService _contragentApiService;

		public ContragentController(ILiteDbContragentService contragentDbService, IDaDataApiContragentService contragentApiService)
		{
			_contragentDbService = contragentDbService;
			_contragentApiService = contragentApiService;
		}

		/// <summary>
		/// Получить список контрагентов. 
		/// Запрос - 'GET api/contragent'
		/// </summary>
		/// <returns>Список контрагентов</returns>
		[HttpGet]
		public IEnumerable<Contragent> Get()
		{
			return _contragentDbService.FindAll();
		}

		/// <summary>
		/// Добавить контрагента. 
		/// Запрос - 'POST api/contragent'
		/// Данные передаются в теле запроса.
		/// </summary>
		/// <param name="contragent"></param>
		[HttpPost]
		public void Post([FromBody] Contragent contragent)
		{
			ContragentType type = contragent.Type.Value;
			string fullName = _contragentApiService.Find(contragent.INN, contragent.KPP, type).Result;
			contragent.FullName = fullName;
			_contragentDbService.Insert(contragent);
		}
	}
}
