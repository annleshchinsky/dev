﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TestRestAPI.Attributes;

namespace TestRestAPI.Models
{
	/// <summary>
	/// Контрагент.
	/// </summary>
	public class Contragent
	{
		/// <summary>
		/// ID
		/// </summary>
		public int? Id { get; set; }

		/// <summary>
		/// Имя
		/// </summary>
		[Required(ErrorMessage = "Укажите имя контрагента.")]
		public string Name { get; set; }

		/// <summary>
		/// Полное имя
		/// </summary>
		public string FullName { get; set; }

		/// <summary>
		/// Тип (юридическое лицо, индивидуальный предприниматель)
		/// </summary>
		[RequiredEnumAttribute(ErrorMessage = "Укажите тип контрагента.")]
		public ContragentType? Type { get; set; }

		/// <summary>
		/// ИНН
		/// </summary>
		[Required(ErrorMessage = "Укажите ИНН контрагента.")]
		[RegularExpression(@"^(\d{10}|\d{12})$", ErrorMessage = "ИНН должен состоять из 10 или 12 цифр.")]
		public string INN { get; set; }

		/// <summary>
		/// КПП
		/// </summary>
		[RequiredForAny(Values = new[] { nameof(ContragentType.LEGAL) }, PropertyName = nameof(Type))]
		[RegularExpression(@"^\d{9}$", ErrorMessage = "КПП должен состоять из 9 цифр.")]
		public string KPP { get; set; }
	}

	/// <summary>
	/// Тип контрагента.
	/// </summary>
	public enum ContragentType
	{
		/// <summary>
		/// Юридическое лицо.
		/// </summary>
		[Description("LEGAL")]
		LEGAL = 0,

		/// <summary>
		/// Индивидуальный предприниматель.
		/// </summary>
		[Description("INDIVIDUAL")]
		INDIVIDUAL = 1
	}
}
