﻿using System.Collections.Generic;
using TestRestAPI.Models;

namespace TestRestAPI.Helpers.LiteDb
{
    /// <summary>
    /// LiteDb contragent service interface.
    /// </summary>
	public interface ILiteDbContragentService
    {
        bool Delete(int id);
        IEnumerable<Contragent> FindAll();
        Contragent FindOne(int id);
        int Insert(Contragent contragent);
        bool Update(Contragent contragent);
    }
}