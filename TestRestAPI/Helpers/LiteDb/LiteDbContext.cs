﻿using LiteDB;
using Microsoft.Extensions.Options;

namespace TestRestAPI.Helpers.LiteDb
{
    /// <summary>
    /// LiteDb context.
    /// </summary>
	public class LiteDbContext : ILiteDbContext
    {
        public LiteDatabase Database { get; }

        public LiteDbContext(IOptions<LiteDbOptions> options)
        {
            Database = new LiteDatabase(options.Value.DatabaseLocation);
        }
    }
}
