﻿namespace TestRestAPI.Helpers.LiteDb
{
    /// <summary>
    /// LiteDb options.
    /// </summary>
	public class LiteDbOptions
    {
        public string DatabaseLocation { get; set; }
    }
}
