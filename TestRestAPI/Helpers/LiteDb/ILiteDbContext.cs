﻿using LiteDB;

namespace TestRestAPI.Helpers.LiteDb
{
    /// <summary>
    /// LiteDb context interface.
    /// </summary>
	public interface ILiteDbContext
    {
        LiteDatabase Database { get; }
    }
}