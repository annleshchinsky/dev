﻿using System.Threading.Tasks;
using TestRestAPI.Models;

namespace TestRestAPI.Helpers.DaData
{
	/// <summary>
	/// DaData api contragent service interface.
	/// </summary>
	public interface IDaDataApiContragentService
	{
		Task<string> Find(string INN, string KPP, ContragentType type);
	}
}
