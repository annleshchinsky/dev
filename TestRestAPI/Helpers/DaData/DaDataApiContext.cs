﻿using Dadata;
using Microsoft.Extensions.Options;

namespace TestRestAPI.Helpers.DaData
{
	/// <summary>
	/// DaData api context.
	/// </summary>
	public class DaDataApiContext : IDaDataApiContext
	{
		public SuggestClientAsync API { get; }

		public DaDataApiContext(IOptions<DaDataApiOptions> options)
		{
			API = new SuggestClientAsync(options.Value.Token);
		}
	}
}
