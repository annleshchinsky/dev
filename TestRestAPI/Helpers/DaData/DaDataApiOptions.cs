﻿namespace TestRestAPI.Helpers.DaData
{
	/// <summary>
	/// DaData api options.
	/// </summary>
	public class DaDataApiOptions
	{
		public string Token { get; set; }
	}
}
