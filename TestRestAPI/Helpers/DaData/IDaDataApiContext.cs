﻿using Dadata;

namespace TestRestAPI.Helpers.DaData
{
	/// <summary>
	/// DaData api context interface.
	/// </summary>
	public interface IDaDataApiContext
	{
		SuggestClientAsync API { get; }
	}
}
