using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TestRestAPI.Helpers.DaData;
using TestRestAPI.Helpers.LiteDb;
using TestRestAPI.Services;

namespace TestRestAPI
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			// LiteDB 
			services.Configure<LiteDbOptions>(Configuration.GetSection("LiteDbOptions"));
			services.AddSingleton<ILiteDbContext, LiteDbContext>();
			services.AddTransient<ILiteDbContragentService, LiteDbContragentService>();

			// DaData
			services.Configure<DaDataApiOptions>(Configuration.GetSection("DaDataApiOptions"));
			services.AddSingleton<IDaDataApiContext, DaDataApiContext>();
			services.AddTransient<IDaDataApiContragentService, DaDataApiContragentService>();

			// Controllers
			services.AddControllers();
			// Swagger
			services.AddSwaggerGen();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseSwagger();

			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "REST Test API V1");

			});

			if (env.IsDevelopment())
			{
				app.UseExceptionHandler("/error-local-development");
			}
			else
			{
				app.UseExceptionHandler("/error");
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
