﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestRestAPI.Attributes
{
    /// <summary>
    /// Required enum value  attribute.
    /// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequiredEnumAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null) return false;
            var type = value.GetType();
            return type.IsEnum && Enum.IsDefined(type, value);
        }
    }
}

