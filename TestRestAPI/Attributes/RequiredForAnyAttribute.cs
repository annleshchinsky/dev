﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TestRestAPI.Attributes
{
    /// <summary>
    /// Conditional required attribute.
    /// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequiredForAnyAttribute : ValidationAttribute
    {
        /// <summary>
        /// Values of the <see cref="PropertyName"/> that will trigger the validation
        /// </summary>
        public string[] Values { get; set; }

        /// <summary>
        /// Independent property name
        /// </summary>
        public string PropertyName { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = validationContext.ObjectInstance;
            if (model == null || Values == null)
            {
                return ValidationResult.Success;
            }
            if (value != null && value.GetType() == typeof(string) && string.IsNullOrEmpty(value.ToString())) value = null;

            var currentValue = model.GetType().GetProperty(PropertyName)?.GetValue(model, null)?.ToString();
            if (Values.Contains(currentValue) && value == null)
            {
                var propertyInfo = validationContext.ObjectType.GetProperty(validationContext.MemberName);
                return new ValidationResult($"Параметр {propertyInfo.Name} обязателен при параметре {PropertyName} равном {currentValue}");
            }
            return ValidationResult.Success;
        }
    }
}
