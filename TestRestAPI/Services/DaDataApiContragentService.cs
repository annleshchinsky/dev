﻿using Dadata;
using Dadata.Model;
using System;
using System.Threading.Tasks;
using TestRestAPI.Helpers.DaData;
using TestRestAPI.Models;

namespace TestRestAPI.Services
{
	/// <summary>
	/// DaData api contragent service.
	/// </summary>
	public class DaDataApiContragentService : IDaDataApiContragentService
	{
		private SuggestClientAsync _API;

		public DaDataApiContragentService(IDaDataApiContext daDataApiContext)
		{
			_API = daDataApiContext.API;
		}
		/// <summary>
		/// Найти контрагента.
		/// </summary>
		/// <param name="INN">ИНН</param>
		/// <param name="KPP">КПП</param>
		/// <param name="type">Тип</param>
		/// <returns>Полное наименование</returns>
		public async Task<string> Find(string INN, string KPP, ContragentType type)
		{
			//throw new NotImplementedException();
			var request = new FindPartyRequest(query: INN, kpp: KPP);
			request.type = (type == ContragentType.INDIVIDUAL) ? PartyType.INDIVIDUAL: PartyType.LEGAL;

			var result = await _API.FindParty(request);
			if (result.suggestions.Count > 0) 
			{
				return result.suggestions[0].data?.name?.full_with_opf;
			}
			else
			{
				throw new ArgumentException("Контрагент с такими параметрами не найден в ЕГРЮЛ.");
			}
		}
	}
}
