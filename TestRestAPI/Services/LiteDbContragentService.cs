﻿using LiteDB;
using System.Collections.Generic;
using System.Linq;
using TestRestAPI.Helpers.LiteDb;
using TestRestAPI.Models;

namespace TestRestAPI.Services
{
    /// <summary>
    /// LiteDb contragent service.
    /// </summary>
	public class LiteDbContragentService : ILiteDbContragentService
    {

        private LiteDatabase _liteDb;

        public LiteDbContragentService(ILiteDbContext liteDbContext)
        {
            _liteDb = liteDbContext.Database;
        }

        /// <summary>
        /// Получить список контрагентов.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Contragent> FindAll()
        {
            var result = _liteDb.GetCollection<Contragent>("Contragent")
                .FindAll();
            return result;
        }

        /// <summary>
        /// Получить контрагента по ID.
        /// </summary>
        /// <param name="id">ID контрагента</param>
        /// <returns></returns>
        public Contragent FindOne(int id)
        {
            return _liteDb.GetCollection<Contragent>("Contragent")
                .Find(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Сохранить контрагента.
        /// </summary>
        /// <param name="contragent">Контрагент</param>
        /// <returns></returns>
        public int Insert(Contragent contragent)
        {
            // Если КПП пустая строка, заменяем на null
            if (string.IsNullOrEmpty(contragent.KPP)) contragent.KPP = null;

            // Проверяем, если контрагент с таким ИНН и КПП уже есть в базе 
            var existingContragent = _liteDb.GetCollection<Contragent>("Contragent")
                .Find(x => (x.INN == contragent.INN && x.KPP == contragent.KPP)).FirstOrDefault();

            // Если дубля в базе нет - сохраняем
            if (existingContragent == null)
            {
                return _liteDb.GetCollection<Contragent>("Contragent")
                    .Insert(contragent);
            }
            else return existingContragent.Id.Value;
        }

        /// <summary>
        /// Обновить контрагента.
        /// </summary>
        /// <param name="contragent"></param>
        /// <returns></returns>
        public bool Update(Contragent contragent)
        {
            return _liteDb.GetCollection<Contragent>("Contragent")
                .Update(contragent);
        }

        /// <summary>
        /// Удалить контрагента.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            return _liteDb.GetCollection<Contragent>("Contragent")
                .Delete(id);
        }
    }
}
